package classesAndObjects;

public class AccountHolderObject {

	public static void main(String[] args) {
		
		AccountHolder tom = new AccountHolder();
		AccountHolder john = new AccountHolder();
		AccountHolder sarah = new AccountHolder();
		
		tom.firstName="Tom";
		tom.lastName="Kaminski";
		tom.age=24;
		tom.accountBalance=10000;
		tom.testeligibleForCreditCard();
		System.out.println("Is Tom eligible for CC: "+tom.eligibleForCreditCard);
		
		john.firstName="John";
		john.lastName="Polanski";
		john.age=26;
		john.accountBalance=19999;
		john.testeligibleForCreditCard();
		System.out.println("Is John eligible for CC: "+john.eligibleForCreditCard);
		
		sarah.firstName="Sarah";
		sarah.lastName="Nowak";
		sarah.age=26;
		sarah.accountBalance=20000;
		sarah.testeligibleForCreditCard();
		System.out.println("Is Sarah eligible for CC: "+sarah.eligibleForCreditCard);
		

	}

}
